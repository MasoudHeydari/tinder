//
//  File.swift
//  TInder
//
//  Created by Masoud Heydari on 8/1/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class HeaderLabel: UILabel {
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.insetBy(dx: 16, dy: 0))
    }
}
