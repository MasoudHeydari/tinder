//
//  UIStackView+Helper.swift
//  TInder
//
//  Created by Masoud Heydari on 8/1/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

extension UIStackView {
    func addArrangedSubview(views: [UIView]) {
         views.forEach {addArrangedSubview($0) }
    }
}
