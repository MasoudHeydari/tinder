//
//  Utils.swift
//  TInder
//
//  Created by Masoud Heydari on 7/30/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class Bindable<T> {
    var value: T? {
        didSet {
            observer?(value)
        }
    }
    
    var observer: ((T?) -> ())?
    
    func bind(observer: @escaping (T?) -> ()) {
        self.observer = observer
    }
}
