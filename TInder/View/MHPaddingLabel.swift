//
//  MHPaddingLabel.swift
//  TInder
//
//  Created by Masoud Heydari on 8/2/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class MHPaddingLabel: UILabel {
    
    let padding: CGFloat
    
    init(padding: CGFloat) {
        self.padding = padding
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.insetBy(dx: padding, dy: 0))
    }
}
