//
// Created by Masoud Heydari on 2019-07-28.
// Copyright (c) 2019 Masoud Heydari. All rights reserved.
//

import UIKit
import SDWebImage

protocol CardViewDelegate: class {
    func didTapMoreInfo(cardViewModel: CardViewModel)
}

class CardView: UIView {
    
    weak var delegate: CardViewDelegate?
    
    private let barDeselectedColor = UIColor(white: 0, alpha: 0.1)
    private let threshold: CGFloat = 80
    
    var cardViewModel: CardViewModel? {
        didSet {
            guard let cardViewModel = cardViewModel else { return }
            let imageName = cardViewModel.imageNames.first ?? ""
            if let url = URL(string: imageName) {
                imageView.sd_setImage(with: url)
            }
            
            informationLabel.attributedText = cardViewModel.attributedString
            informationLabel.textAlignment = cardViewModel.textAlignment
            
            let numbers = cardViewModel.imageNames.count
            setupBarStackViewArrangedSubviews(numbers: numbers)
            
            setupImageIndexObserver()
        }
    }
    
    let barStackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    private let imageView: UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "messi3"))
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    private let gradientLayer: CAGradientLayer = {
        let gl = CAGradientLayer()
        let startColor = UIColor.clear.cgColor
        let endColor = UIColor.black.cgColor
        gl.colors = [startColor, endColor]
        gl.locations = [0.5, 1.1]
        return gl
    }()
    
    private let informationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    private let btnMoreInfo: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(#imageLiteral(resourceName: "more_info").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(handleBtnMoreInfoTapped), for: .touchUpInside)
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        setupGestures()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientLayerFrame()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        layer.cornerRadius = 8
        clipsToBounds = true
        
        addSubviewsToMainView()
        setupGradientLayer()
        setupImageView()
        setupBarsStackView()
        setupInformationLabel()
        setupBtnMoreInfo()
    }
    
    private func addSubviewsToMainView() {
        addSubview(imageView)
        addSubview(barStackView)
    }
    
    private func setupBarsStackView() {
        barStackView.spacing = 4
        barStackView.distribution = .fillEqually
        barStackView.constrainHeight(constant: Size.View.CardView.barViewHeight)
        
        [barStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
         barStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
         barStackView.topAnchor.constraint(equalTo: topAnchor, constant: 8)].forEach{$0.isActive = true}
        
    }
    
    private func setupImageView() {
        imageView.fillToSuperview()
    }
    
    private func setupGradientLayer() {
        layer.addSublayer(gradientLayer)
    }
    
    private func updateGradientLayerFrame() {
        gradientLayer.frame = self.frame
    }
    
    private func setupInformationLabel() {
        addSubview(informationLabel)
        addSubview(btnMoreInfo)
        
        [informationLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
         informationLabel.rightAnchor.constraint(equalTo: btnMoreInfo.leftAnchor, constant: -16),
         informationLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)].forEach{$0.isActive = true}
    }
    
    private func setupBtnMoreInfo() {
        btnMoreInfo.constrainWidth(constant: 40)
        btnMoreInfo.constrainHeight(constant: 40)
        
        [btnMoreInfo.rightAnchor.constraint(equalTo: rightAnchor, constant: -16),
         btnMoreInfo.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)].forEach{$0.isActive = true}
    }
    
    private func setupGestures() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        
        addGestureRecognizer(panGesture)
        addGestureRecognizer(tapGesture)
    }
    
    private func setupImageIndexObserver() {
        guard let cardViewModel = self.cardViewModel else { return }
        
        cardViewModel.imageIndexObserver = { [weak self] (index, imageUrl) in
            
            guard let strongSelf = self, let imageUrl = imageUrl else { return }
            
            strongSelf.imageView.sd_setImage(with: URL(string: imageUrl))
            strongSelf.barStackView.arrangedSubviews.forEach({ (barView) in
                barView.backgroundColor = strongSelf.barDeselectedColor
            })
            
            // change the color of selected barView to white
            strongSelf.barStackView.arrangedSubviews[index].backgroundColor = .white
            
        }
    }
    
    private func setupBarStackViewArrangedSubviews(numbers: Int) {
        (0..<numbers).forEach { (_) in
            let barView = UIView()
            barView.cornerRadius = Size.View.CardView.barViewCornerRadius
            barView.backgroundColor = barDeselectedColor
            barStackView.addArrangedSubview(barView)
        }
        
        barStackView.arrangedSubviews.first?.backgroundColor = .white
    }
    
    private func panGestureBegan() {
        superview?.subviews.forEach({ (subview) in
            subview.layer.removeAllAnimations()
        })
    }
    
    private func panGestureChanged(_ gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        let degree: CGFloat = translation.x / 20
        let angle = degree * .pi / 180
        
        let rotationlaTransformation = CGAffineTransform(rotationAngle: angle)
        self.transform = rotationlaTransformation.translatedBy(x: translation.x, y: translation.y)
    }
    
    private func panGestureEnded(_ gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        let translationDirection: CGFloat = translation.x > 0 ? 1 : -1
        let shouldDissCard = abs(translation.x) > threshold
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.1, options: .curveEaseOut, animations: {
            if shouldDissCard {
                self.frame = .init(x: 600 * translationDirection, y: 0, width: self.frame.width, height: self.frame.height)
            } else {
                self.transform = .identity
            }
            
        }) { (_) in
            self.transform = .identity
            if shouldDissCard {
                self.removeFromSuperview()
            }
        }
    }
    
    @objc private func handlePanGesture(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            panGestureBegan()
        case .changed:
            panGestureChanged(gesture)
        case .ended:
            panGestureEnded(gesture)
        default:
            ()
        }
    }
    
    @objc private func handleTapGesture(gesture: UITapGestureRecognizer) {
        guard let cardViewModel = self.cardViewModel else { return }
        let tapLocation = gesture.location(in: nil).x
        cardViewModel.shouldAdvanceNextPhoto(point: tapLocation, frameWidth: frame.width)
    }
    
    @objc private func handleBtnMoreInfoTapped() {
        guard let cardViewModel = self.cardViewModel else { return }
        delegate?.didTapMoreInfo(cardViewModel: cardViewModel)
    }
}
