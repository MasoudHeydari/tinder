//
//  AgeRangeCell.swift
//  TInder
//
//  Created by Masoud Heydari on 8/2/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class AgeRangeCell: UITableViewCell {
    
    let minSlider = MHSlider(minValue: 18, maxValue: 100)
    let maxSlider = MHSlider(minValue: 18, maxValue: 100)
    
    let minLabel: MHPaddingLabel = {
        let label = MHPaddingLabel(padding: 0)
        label.text = "Min"
        label.numberOfLines = 1
        label.sizeToFit()
        return label
    }()
    
    let maxLabel: MHPaddingLabel = {
        let label = MHPaddingLabel(padding: 0)
        label.text = "Max"
        label.sizeToFit()
        label.numberOfLines = 1
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    private func setup() {
        setupOverallStackView()
    }
    
    private func setupOverallStackView() {
        let minStackView = UIStackView(arrangedSubviews: [minLabel, minSlider])
        let maxStackView = UIStackView(arrangedSubviews: [maxLabel, maxSlider])
        
        minStackView.spacing = 6
        maxStackView.spacing = 6
        
        // let's draw a vertical stack view of sliders right now
        let overallStackView = UIStackView(arrangedSubviews: [
            minStackView,
            maxStackView,
            ])
        
        overallStackView.translatesAutoresizingMaskIntoConstraints = false
        overallStackView.axis = .vertical
        overallStackView.spacing = 16
        addSubview(overallStackView)
        
        [overallStackView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
         overallStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
         overallStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16),
         overallStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)]
            .forEach{$0.isActive = true}
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
