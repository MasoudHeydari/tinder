//
// Created by Masoud Heydari on 2019-07-28.
// Copyright (c) 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class HomeTopNavigationStackView: UIStackView {
    
    let btnSettings: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "top_left_profile").withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    
    let btnMessage: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "top_right_messages").withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    
    let fireImageView: UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "fire_img"))
        imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        constrainHeight(constant: Size.View.HomeTopNavigationStackView.stackViewHeight)
        setupArrangeSubviews()
        distribution = .equalCentering
        isLayoutMarginsRelativeArrangement = true
        layoutMargins = Size.View.HomeTopNavigationStackView.stackViewMargin
        
    }
    
    private func setupArrangeSubviews() {
        [btnSettings, SpacerView(), fireImageView, SpacerView(), btnMessage].forEach{addArrangedSubview($0)}
    }
}
