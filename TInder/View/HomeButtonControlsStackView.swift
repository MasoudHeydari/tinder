//
// Created by Masoud Heydari on 2019-07-28.
// Copyright (c) 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class HomeButtonControlsStackView: UIStackView {
    
    let btnRefresh: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "refresh_circle").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFill
        return btn
    }()
    
    let btnDislike: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "dismiss_circle").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFill
        return btn
    }()
    
    let btnSuperLike: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "super_like_circle").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFill
        return btn
    }()
    
    let btnLike: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "like_circle").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFill
        return btn
    }()
    
    let btnSpecial: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "boost_circle").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFill
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        distribution = .fillEqually
        constrainHeight(constant: Size.View.HomeButtonControlsStackView.stackViewHeight)
        setupArrangeSubviews()
    }
    
    private func setupArrangeSubviews() {
        [btnRefresh, btnDislike, btnSuperLike, btnLike, btnSpecial].forEach { (btn) in
            btn.imageView?.image?.withRenderingMode(.alwaysOriginal)
            addArrangedSubview(btn)
        }
    }
}
