//
//  RegistrationController.swift
//  Tinder
//
//  Created by Masoud Heydari on 7/27/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit
import Firebase
import JGProgressHUD

class RegistrationController: UIViewController {
    
    //TODO: fix the auto layout issues.
    
    weak var delegate: LoginControllerDelegate?
    
    lazy var selectPhotoButtonHeightAnchor = btnSelectPhoto.heightAnchor.constraint(equalToConstant: Size.Controller.Register.btnSelectPhotoHeight)
    lazy var selectPhotoButtonWidthAnchor = btnSelectPhoto.widthAnchor.constraint(equalToConstant:  Size.Controller.Register.btnSelectPhotoWidth)
    
    lazy var overallStackViewTopAnchor = overallStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50)
    lazy var overallStackViewBottomAnchor = overallStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -50)
    
    
    private let registrationViewModel = RegistrationViewModel()
    private var imagePickerController: UIImagePickerController?
    private let progressHUD = JGProgressHUD(style: .dark)
    private let spacerView = SpacerView()
    
    let overallStackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.spacing = 8
        return sv
    }()
    
    let textFieldsAndRegisterBtnStackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.axis = .vertical
        sv.distribution = .fillEqually
        sv.spacing = 8
        return sv
    }()
    
    let gradientLayer: CAGradientLayer = {
        let gl = CAGradientLayer()
        let startColor = #colorLiteral(red: 0.9921568627, green: 0.3568627451, blue: 0.3725490196, alpha: 1).cgColor
        let endColor = #colorLiteral(red: 0.8980392157, green: 0, blue: 0.4470588235, alpha: 1).cgColor
        gl.locations = [0, 1]
        gl.colors = [startColor, endColor]
        return gl
    }()
    
    let fullNameTextField: CustomTextField = {
        let tv = CustomTextField(padding: 18, height: Size.Controller.Register.textFieldsHeight)
        tv.backgroundColor = .white
        tv.placeholder = "Enter Full Name"
        tv.addPaddingLeft(18)
        tv.addPaddingRight(18)
        return tv
    }()
    
    let emailTextField: CustomTextField = {
        let tv = CustomTextField(padding: 18, height: Size.Controller.Register.textFieldsHeight)
        tv.backgroundColor = .white
        tv.placeholder = "Enter Your Email"
        tv.addPaddingLeft(18)
        tv.addPaddingRight(18)
        return tv
    }()
    
    let passwordTextField: CustomTextField = {
        let tv = CustomTextField(padding: 18, height: 0)
        tv.backgroundColor = .white
        tv.isSecureTextEntry = true
        tv.placeholder = "Enter Your Password"
        tv.addPaddingLeft(18)
        tv.addPaddingRight(18)
        return tv
    }()
    
    let btnSelectPhoto: UIButton = {
        let btn = UIButton(type: .system)
        btn.layer.cornerRadius = 16
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 32, weight: .heavy)
        btn.backgroundColor = .white
        btn.setTitle("Select Photo", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.imageView?.contentMode = .scaleAspectFill
        btn.addTarget(self, action: #selector(handleBtnSelectPhotoTapped), for: .touchUpInside)
        btn.clipsToBounds = true
        return btn
    }()
    
    let btnRegister: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        btn.backgroundColor = .lightGray
        btn.setTitle("Register", for: .normal)
        btn.setTitleColor(.gray, for: .disabled)
        btn.addTarget(self, action: #selector(handleBtnRegistereTapped), for: .touchUpInside)
        btn.isEnabled = false
        return btn
    }()
    
    let btnGoToLogin: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Go to Login", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        btn.addTarget(self, action: #selector(handleBtnLoginTapped), for: .touchUpInside)
        return btn
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupRegisterViewModelBindables()
        setupImagePickerController()
    }
    
    private func setupView() {
        setupGradientLayer()
        setupOverallStackView()
        setupTextFields()
        setupBtnGoToLogin()
    }
    
    private func setupBtnGoToLogin() {
        view.addSubview(btnGoToLogin)
        
        [btnGoToLogin.leftAnchor.constraint(equalTo: view.leftAnchor),
         btnGoToLogin.rightAnchor.constraint(equalTo: view.rightAnchor),
         btnGoToLogin.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)]
            .forEach{$0.isActive = true}
    }
    
    private func setupImagePickerController() {
        imagePickerController = UIImagePickerController()
        imagePickerController?.delegate = self
    }
    
    private func setupOverallStackView() {
        [fullNameTextField, emailTextField, passwordTextField, btnRegister, ].forEach {
            textFieldsAndRegisterBtnStackView.addArrangedSubview($0)
        }
        [btnSelectPhoto, textFieldsAndRegisterBtnStackView].forEach {
            overallStackView.addArrangedSubview($0)
        }
        edgesForExtendedLayout = []
        
        
        view.addSubview(overallStackView)
        overallStackView.isLayoutMarginsRelativeArrangement = true
        overallStackView.layoutMargins = .init(top: 0, left: 0, bottom: 0, right: 0)
        
        [overallStackView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 50),
         overallStackView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -50),
         overallStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor)]
            .forEach{$0.isActive = true}
        
    }
    
    private func setupTextFields() {
        [fullNameTextField, emailTextField, passwordTextField].forEach {
            $0.addTarget(self, action: #selector(handleTextFieldsChange), for:.editingChanged)
        }
    }
    
    private func setupRegisterViewModelBindables() {
        setupViewModelBindableIsFormValid()
        setupViewModelBindableImage()
        setupViewModelBindableIsRegistering()
    }
    
    private func setupViewModelBindableIsFormValid() {
        registrationViewModel.bindableIsFormValid.bind(observer: { [weak self] (isValid) in
            guard let strongSelf = self, let isValid = isValid else { return }
            strongSelf.btnRegister.isEnabled = isValid
            if isValid {
                strongSelf.btnRegister.backgroundColor = #colorLiteral(red: 0.8235294118, green: 0, blue: 0.3254901961, alpha: 1)
                strongSelf.btnRegister.setTitleColor(.white, for: .normal)
            } else {
                strongSelf.btnRegister.backgroundColor = .lightGray
                strongSelf.btnRegister.setTitleColor(.gray, for: .normal)
            }
            
        })
    }
    
    private func setupViewModelBindableImage() {
        registrationViewModel.bindableImage.bind(observer: { [weak self] (image) in
            guard let strongSelf = self, let image = image else { return }
            let originalImage = image.withRenderingMode(.alwaysOriginal)
            strongSelf.btnSelectPhoto.setImage(originalImage, for: .normal)
        })
    }
    
    private func setupViewModelBindableIsRegistering() {
        registrationViewModel.bindableIsRegistering.bind { [weak self] (isRegistering) in
            guard let strongSelf = self else { return }
            guard let isRegistering = isRegistering else { return }
            if isRegistering {
                strongSelf.progressHUD.textLabel.text = "Registering"
                strongSelf.progressHUD.show(in: strongSelf.view)
            } else {
                strongSelf.progressHUD.dismiss()
            }
        }
    }
    
    private func setupGradientLayer() {
        view.layer.addSublayer(gradientLayer)
    }
    
    private func updateGradientLayerFrame() {
        gradientLayer.frame = view.bounds
    }
    
    private func updateTextFieldsAndRegisterButtonCornerRadius() {
        overallStackView.layoutIfNeeded()
        let cornerRadius = fullNameTextField.frame.height / 2
        [fullNameTextField, emailTextField, passwordTextField, btnRegister].forEach {
            $0.cornerRadius = cornerRadius
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        if traitCollection.horizontalSizeClass == .compact && traitCollection.verticalSizeClass == .regular {
            // device is in portrait mode

            selectPhotoButtonHeightAnchor.isActive = true
            selectPhotoButtonWidthAnchor.isActive = false
            
            overallStackViewTopAnchor.isActive = false
            overallStackViewBottomAnchor.isActive = false
            
            setupEssentialViewInPortraitMode()
        } else {
            // device is in landscape mode
            
            overallStackViewTopAnchor.isActive = true
            overallStackViewBottomAnchor.isActive = true
            
            selectPhotoButtonHeightAnchor.isActive = false
            selectPhotoButtonWidthAnchor.isActive = true
            
            print("img height \(selectPhotoButtonWidthAnchor.constant)")
            
            
            setupEssentialViewInLandscapeMode()
        }
    }
    
    private func setupEssentialViewInPortraitMode() {
        overallStackView.axis = .vertical
        textFieldsAndRegisterBtnStackView.addArrangedSubview(spacerView)
        
        updateGradientLayerFrame()
        updateTextFieldsAndRegisterButtonCornerRadius()
    }
    
    private func setupEssentialViewInLandscapeMode() {
        overallStackView.axis = .horizontal
        guard let spacerView = textFieldsAndRegisterBtnStackView.arrangedSubviews.last else { return }
        canHideSpacerView(spacerView)
        // check weather can hide the spacer view or not via ViewModel
        registrationViewModel.textFieldsAndButtonCount = textFieldsAndRegisterBtnStackView.arrangedSubviews.count
        
        updateGradientLayerFrame()
        updateTextFieldsAndRegisterButtonCornerRadius()
    }
    
    private func canHideSpacerView(_ spacerView: UIView) {
        registrationViewModel.bindableCanHideSpacerView.bind(observer: { (canHide) in
            guard let canHide = canHide else { return }
            canHide ? spacerView.removeFromSuperview() : ()
        })
    }
    
    private func showProgressHUDWithError(error: Error) {
        self.progressHUD.dismiss()
        let errorHUD = JGProgressHUD(style: .dark)
        errorHUD.textLabel.text = Const.HUDTitle.failed_to_registration
        errorHUD.detailTextLabel.text = error.localizedDescription
        errorHUD.show(in: self.view)
        errorHUD.dismiss(afterDelay: 4)
    }
    
    @objc private func handleTextFieldsChange(textField: UITextField) {
        switch textField {
        case fullNameTextField:
            registrationViewModel.fullName = textField.text
        case emailTextField:
            registrationViewModel.email = textField.text
        case passwordTextField:
            registrationViewModel.password = textField.text
        default:
            ()
        }
    }
    
    @objc private func handleBtnSelectPhotoTapped(sender: UIButton) {
        guard let imagePickerController = imagePickerController else { return }
        imagePickerController.delegate = self
        present(imagePickerController, animated: true)
    }
    
    @objc private func handleBtnRegistereTapped(_ sender: UIButton) {
        registrationViewModel.performRegistration { [weak self] (error) in
            guard let strongSelf = self else { return }
            if let error = error {
                strongSelf.showProgressHUDWithError(error: error)
                return
            }
            
            // there no error
            print("User successfully registered")
            strongSelf.dismiss(animated: true, completion: {
                strongSelf.delegate?.didFinishLoggingIn()
            })        }
    }
    
    @objc private func handleBtnLoginTapped() {
        let loginController = LoginController()
//        loginController.delegate = delegate
        navigationController?.pushViewController(loginController, animated: true)
    }
}

extension RegistrationController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        registrationViewModel.bindableImage.value = image
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
}
