//
//  File.swift
//  TInder
//
//  Created by Masoud Heydari on 8/2/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import Foundation

protocol LoginControllerDelegate: class {
    func didFinishLoggingIn()
}
