//
//  LoginController.swift
//  TInder
//
//  Created by Masoud Heydari on 8/2/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit
import JGProgressHUD

class LoginController: UIViewController {
    
    weak var delegate: LoginControllerDelegate?
    
    private let loginViewModel = LoginViewModel()
    private let loginHUD = JGProgressHUD(style: .dark)
    
    private let gradientLayer: CAGradientLayer = {
        let gl = CAGradientLayer()
        
        let topColor = #colorLiteral(red: 0.9921568627, green: 0.3568627451, blue: 0.3725490196, alpha: 1).cgColor
        let bottomColor = #colorLiteral(red: 0.8980392157, green: 0, blue: 0.4470588235, alpha: 1).cgColor
        
        gl.colors = [topColor, bottomColor]
        gl.locations = [0, 1]
        
        return gl
    }()

    let emailTextField: CustomTextField = {
        let tf = CustomTextField(padding: 24, height: 50)
        tf.placeholder = "Enter email"
        tf.keyboardType = .emailAddress
        tf.addTarget(self, action: #selector(handleInputTextChange), for: .editingChanged)
        return tf
    }()
    
    let passwordTextField: CustomTextField = {
        let tf = CustomTextField(padding: 24, height: 50)
        tf.placeholder = "Enter password"
        tf.isSecureTextEntry = true
        tf.addTarget(self, action: #selector(handleInputTextChange), for: .editingChanged)
        return tf
    }()
    
    lazy var verticalStackView: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [
            emailTextField,
            passwordTextField,
            btnLogin
            ])
        sv.axis = .vertical
        sv.spacing = 8
        return sv
    }()
    
    let btnLogin: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Login", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        btn.backgroundColor = .lightGray
        btn.setTitleColor(.gray, for: .disabled)
        btn.isEnabled = false
        btn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        btn.layer.cornerRadius = 22
        btn.addTarget(self, action: #selector(handleBtnLoginTapped), for: .touchUpInside)
        return btn
    }()
    
    private let btnBackToRegister: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Go back", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btn.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGradientLayer()
        setupView()
        setupBindables()
    }
    
    private func setupBindables() {
        setupIsFormValidBindable()
        setupIsLoggingInBindable()
    }
    
    private func setupIsFormValidBindable() {
        loginViewModel.isFormValid.bind { [unowned self] (isFormValid) in
            guard let isFormValid = isFormValid else { return }
            self.btnLogin.isEnabled = isFormValid
            self.btnLogin.backgroundColor = isFormValid ? #colorLiteral(red: 0.8235294118, green: 0, blue: 0.3254901961, alpha: 1) : .lightGray
            self.btnLogin.setTitleColor(isFormValid ? .white : .gray, for: .normal)
        }
    }
    
    private func setupIsLoggingInBindable() {
        loginViewModel.isLoggingIn.bind { [unowned self] (isRegistering) in
            if isRegistering == true {
                self.loginHUD.textLabel.text = "Register"
                self.loginHUD.show(in: self.view)
            } else {
                self.loginHUD.dismiss()
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateGradientLayerFrame()
    }
    
    private func setupGradientLayer() {
        view.layer.addSublayer(gradientLayer)
        updateGradientLayerFrame()
    }
    
    private func setupView() {
        navigationController?.isNavigationBarHidden = true
        view.addSubview(verticalStackView)
        
        
        
        
        [verticalStackView.leftAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
         verticalStackView.rightAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
         verticalStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor)]
            .forEach{$0.isActive = true}
        
        
        view.addSubview(btnBackToRegister)
        
        
        [btnBackToRegister.leftAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
         btnBackToRegister.rightAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
         btnBackToRegister.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)]
            .forEach{$0.isActive = true}
        
    }
    
    private func updateGradientLayerFrame() {
        gradientLayer.frame = view.bounds
    }
    
    @objc private func handleInputTextChange(textField: UITextField) {
        if textField == emailTextField {
            loginViewModel.email = textField.text
        } else {
            loginViewModel.password = textField.text
        }
    }
    
    @objc private func handleBtnLoginTapped() {
        loginViewModel.performLogin { (err) in
            self.loginHUD.dismiss()
            if let err = err {
                print("Failed to log in:", err)
                return
            }
            
            print("Logged in successfully")
            self.dismiss(animated: true, completion: {
                self.delegate?.didFinishLoggingIn()
            })
        }
    }
    
    @objc private func handleBack() {
        navigationController?.popViewController(animated: true)
    }
    
}
