//
//  Size.swift
//  TInder
//
//  Created by Masoud Heydari on 7/27/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

struct Size {
    
    struct View {
        static let width: CGFloat = {
            if UIApplication.shared.statusBarOrientation.isPortrait {
                // device is in portrait mode
                return UIScreen.main.bounds.width
            }
            return UIScreen.main.bounds.height
        }()
        
        static let height: CGFloat = {
            if UIApplication.shared.statusBarOrientation.isPortrait {
                // device is in portrait mode
                return UIScreen.main.bounds.height
            }
            return UIScreen.main.bounds.width
        }()

        struct HomeTopNavigationStackView {
            static let stackViewHeight: CGFloat = 80
            static let stackViewMargin = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        }
        
        struct HomeButtonControlsStackView {
            static let stackViewHeight: CGFloat = 100
        }
        
        struct CardView {
            static let barViewHeight: CGFloat = 3
            static let barViewCornerRadius: CGFloat = barViewHeight / 2
        }
        
    }
    
    
    struct Controller {
        
        struct Home {
            static let overallStackViewMargin = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        }
        
        struct Register {
            static let btnSelectPhotoHeight: CGFloat = View.width / 4 * 3
            static let btnSelectPhotoWidth: CGFloat = View.width / 4 * 3
            static let btnSelectPhotoCornerRadius: CGFloat = 18
            static let textFieldsHeight: CGFloat = 50
            
            static let safeAreaInset = UIApplication.shared.keyWindow?.safeAreaInsets
            static let textFieldsAndRegisterBtnSV: CGFloat = UIScreen.main.bounds.width - CGFloat(safeAreaInset?.left ?? 0) - CGFloat(safeAreaInset?.right ?? 0) - btnSelectPhotoHeight - 108
        }
    }
    
}
