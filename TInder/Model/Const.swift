//
//  Const.swift
//  TInder
//
//  Created by Masoud Heydari on 7/27/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

struct Const {
    static let empty = ""
    
    struct HUDTitle {
        static let failed_to_registration = "Failed to Registeration"
    }
    
}
